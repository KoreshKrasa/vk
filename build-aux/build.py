#!/usr/bin/python3

import sys
from os import environ, path
from subprocess import call

BUILD_DIR = sys.argv[1]
SOURCE_DIR = sys.argv[2]
OUTPUT_NAME = sys.argv[3]

environ['CARGO_TARGET_DIR'] = f'{BUILD_DIR}/target'

print('** RUST VERSION **')
call(['rustc', '--version'])

print('** BUILDING APPLICATION **')
call(['cargo', 'build', '--manifest-path', f'{SOURCE_DIR}/Cargo.toml'])
call(['cp', f'{BUILD_DIR}/target/debug/template', OUTPUT_NAME])
