#[macro_use]
extern crate log;
#[macro_use]
extern crate glib;
#[macro_use]
extern crate gtk_macros;
use gio;

mod application;
mod config;
mod window;
mod views;

fn main() {
    glib::set_application_name("Template");
    glib::set_prgname(Some("krasa-template"));
    
    // Load gresources
    let res = gio::Resource::load(
        config::PKGDATADIR.to_owned() + &format!("/{}.gresource", config::APP_ID),
    )
    .expect("Could not load resources");
    gio::resources_register(&res);
    
    gtk::init();
    libadwaita::init();

    info!("Starting Application");
    // let app = application::Application::new();
    // app.run();
    application::VkApplication::run();
}
