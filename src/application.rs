use once_cell::unsync::OnceCell;
use glib::subclass;
use glib::subclass::prelude::*;
use glib::WeakRef;
use gio::subclass::prelude::ApplicationImpl;
use gio::{self, prelude::*, ApplicationFlags};

use gio::prelude::*;
use glib::prelude::*;
use gtk::prelude::*;
use libadwaita::prelude::*;
use gtk::subclass::application::GtkApplicationImpl;
use std::env;

use crate::config;
use crate::window::{ApplicationWindow, Window};
pub struct Application {
    application: gtk::Application,
    window: gtk::Window,
}

pub struct VkApplicationPricvate {
    window: OnceCell<WeakRef<ApplicationWindow>>,
}

impl ObjectSubclass for VkApplicationPricvate {
    const NAME: &'static str = "VkApplication";
    type Type = VkApplication;
    type ParentType = gtk::Application;
    type Interfaces = ();
    type Instance = subclass::simple::InstanceStruct<Self>;
    type Class = subclass::simple::ClassStruct<Self>;

    glib::object_subclass!();

    fn new() -> Self {
        let window = OnceCell::new();

        Self { window }
    }
}

// Implement GLib.OBject for SoukApplication
impl ObjectImpl for VkApplicationPricvate {}

// Implement Gtk.Application for SoukApplication
impl GtkApplicationImpl for VkApplicationPricvate {}

// Implement Gio.Application for SoukApplication
impl ApplicationImpl for VkApplicationPricvate {
    fn activate(&self, _app: &VkApplication) {
        debug!("Activate GIO Application...");

        // If the window already exists,
        // present it instead creating a new one again.
        if let Some(weak_win) = self.window.get() {
            let window = weak_win.upgrade().unwrap();
            window.present();
            info!("Application window presented.");
            return;
        }

        // No window available -> we have to create one
        let app = ObjectSubclass::get_instance(self)
            .downcast::<VkApplication>()
            .unwrap();

        // debug!("Setup Souk base components...");
        // app.setup();
        // debug!("Souk base components are ready.");

        debug!("Create new application window...");
        let window = app.create_window();
        window.present();
        self.window.set(window.downgrade()).unwrap();
        info!("Created application window.");

        // Setup action channel
        // let receiver = self.receiver.borrow_mut().take().unwrap();
        // receiver.attach(None, move |action| app.process_action(action));
    }
}

// Wrap SoukApplicationPrivate into a usable gtk-rs object
glib::wrapper! {
    pub struct VkApplication(ObjectSubclass<VkApplicationPricvate>)
    @extends gio::Application, gtk::Application;
}

impl VkApplication {
    pub fn run() {
        let app = glib::Object::new::<Self>(&[
            ("application-id", &Some(config::APP_ID)),
            ("flags", &ApplicationFlags::empty()),
        ])
        .unwrap();

        app.set_default();
        app.set_resource_base_path(Some("/com/github/koreshkrasa/vk"));

        let args: Vec<String> = env::args().collect();
        ApplicationExtManual::run(&app, &args);
    }

    fn create_window(&self) -> ApplicationWindow{
        let window = ApplicationWindow::new(self.clone());
        window
    }
}



impl Application {
    pub fn new() -> Self {
        let application = Self::create_application();
        let window = gtk::Window::new(); //Self::create_application_window(&application);
        let app = Self {
            application,
            window,
        };
        app.setup_signals();
        app
    }

    fn setup_signals(&self) {
        // let _window = gtk::Window::new();
        self.application
            .connect_activate(clone!(@weak self.window as window => move |app| {
                window.set_application(Some(app));
                app.add_window(&window);
                window.present();
            }));
    }

    fn create_application() -> gtk::Application {
        let app_id = Some(config::APP_ID);
        let flags = gio::ApplicationFlags::FLAGS_NONE;
        gtk::Application::new(app_id, flags).unwrap()
    }

    fn create_application_window(app: &gtk::Application) {
        // ApplicationWindow::new(app)
    }
    pub fn run(&self) {
        let args: Vec<String> = env::args().collect();
        self.application.run(&args);
    }
}
