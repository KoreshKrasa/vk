use glib;
use gtk::prelude::InitializingWidgetExt;
use gtk::subclass::prelude::*;
use gtk::prelude::*;
use gtk::Application;
use libadwaita;
pub struct Window {
    pub widget: libadwaita::ApplicationWindow,
}
use crate::views::LoginPage;
use gtk::ButtonExt;
use gtk::WidgetExt;
use once_cell::unsync::OnceCell;

use std::cell::RefCell;
use std::env;
use std::rc::Rc;
use crate::application;
enum View {
    Timeline,
    Messages,
}

#[derive(Debug, gtk::CompositeTemplate)]
#[template(resource = "/com/github/koreshkrasa/vk/ui/window.ui")]
pub struct TemplateApplicationWindowPrivate {
    #[template_child]
    pub app_stack: TemplateChild<gtk::Stack>,
    #[template_child]
    pub window_stack: TemplateChild<gtk::Stack>,
    #[template_child]
    pub login_box: TemplateChild<gtk::Box>,
    pub login_page: OnceCell<Rc<LoginPage>>,
}

impl ObjectSubclass for TemplateApplicationWindowPrivate {
    const NAME: &'static str = "TemaplteApplicationWindow";
    type Type = ApplicationWindow;
    type ParentType = libadwaita::ApplicationWindow;
    type Interfaces = ();
    type Instance = glib::subclass::simple::InstanceStruct<Self>;
    type Class = glib::subclass::simple::ClassStruct<Self>;
    glib::object_subclass!();

    fn new() -> Self {
        Self {
            app_stack: TemplateChild::default(),
            window_stack: TemplateChild::default(),
            login_box: TemplateChild::default(),
            login_page: OnceCell::new(),
        }
    }

    fn class_init(klass: &mut Self::Class) {
        Self::bind_template(klass);
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self::Type>) {
        obj.init_template();
    }
}

// Implement GLib.OBject for SoukApplicationWindow
impl ObjectImpl for TemplateApplicationWindowPrivate {}

// Implement Gtk.Widget for SoukApplicationWindow
impl WidgetImpl for TemplateApplicationWindowPrivate {}

// Implement Gtk.Window for SoukApplicationWindow
impl WindowImpl for TemplateApplicationWindowPrivate {}

// Implement Gtk.ApplicationWindow for SoukApplicationWindow
impl gtk::subclass::prelude::ApplicationWindowImpl for TemplateApplicationWindowPrivate {}

// Implement Adw.ApplicationWindow for SoukApplicationWindow
impl libadwaita::subclass::prelude::AdwApplicationWindowImpl for TemplateApplicationWindowPrivate {}

// Wrap TemplateApplicationWindowPrivate into a usable gtk-rs object
glib::wrapper! {
    pub struct ApplicationWindow(ObjectSubclass<TemplateApplicationWindowPrivate>)
    @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, libadwaita::ApplicationWindow;
}

impl ApplicationWindow {
    pub fn new(app: application::VkApplication) -> Self {
        let win = glib::Object::new::<Self>(&[("application", &app)])
            .expect("Failed to create SolanumWindow");

        // win.init();
        // win.setup_actions();
        win.setup_ui();
        win.setup_widgets();
        win.setup_signals();
        // Set icons for shell
        // gtk::Window::set_default_icon_name(config::APP_ID);

        win
    }

    fn setup_signals(&self) {
        let self_ = TemplateApplicationWindowPrivate::from_instance(self);

        // main stack
        self_.app_stack.connect_property_visible_child_notify(
            clone!(@weak self as this => move |app_stack| {
                let view = match app_stack.get_visible_child_name().unwrap().as_str(){
                    "timeline" => View::Timeline,
                    "messages" => View::Messages,
                    _ => View::Timeline,
                };
                this.set_view(view, false);
            }),
        );
    }

    fn setup_widgets(&self) {
        let self_ = TemplateApplicationWindowPrivate::from_instance(self);
        self_
            .login_box
            .append(&self_.login_page.get().unwrap().widget);
    }

    fn setup_ui(&self) {
        let self_ = TemplateApplicationWindowPrivate::from_instance(self);
        let login_page = LoginPage::new();
        let _ = self_.login_page.set(login_page);
    }

    fn set_view(&self, view: View, go_back: bool) {
        let self_ = TemplateApplicationWindowPrivate::from_instance(self);

        let app_stak = &*self_.app_stack;
        match view {
            View::Timeline => {
                app_stak.set_visible_child_name("timeline");
            }
            View::Messages => {
                app_stak.set_visible_child_name("messages");
            }
        }
    }
}
