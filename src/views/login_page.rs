use gio::prelude::*;
use gtk::prelude::*;

use std::fmt;
use std::rc::Rc;

use crate::config;

#[derive(Debug)]
pub struct LoginPage {
    pub widget: gtk::Box,

    builder: gtk::Builder,
}

impl LoginPage {
    pub fn new() -> Rc<Self> {
        let builder = gtk::Builder::from_resource(format!(
            "{}/ui/views/login_page.ui",
            config::RESOURCE_ROOT
        ).as_str());
        get_widget!(builder, gtk::Box, login_page);

        let login_page = Rc::new(Self {
            widget: login_page,
            builder,
        });

        // loading_page.clone().setup_widgets();
        // loading_page.clone().setup_signals();
        login_page
    }

    fn setup_widgets(self: Rc<Self>) {
        // get_widget!(self.builder, gtk::Image, image);
        // image.set_from_icon_name(Some(&config::APP_ID));
        // image.set_pixel_size(192);
    }

    fn setup_signals(self: Rc<Self>) {
        // self.database.connect_local("populating-started", false, clone!(@strong self.sender as sender => @default-return None::<glib::Value>, move |_|{
        //     send!(sender, Action::ViewSet(View::Loading));
        //     None
        // })).unwrap();

        // self.database.connect_local("populating-ended", false, clone!(@strong self.sender as sender => @default-return None::<glib::Value>, move |_|{
        //     send!(sender, Action::ViewSet(View::Explore));
        //     None
        // })).unwrap();

        // get_widget!(self.builder, gtk::ProgressBar, progressbar);
        // self.database.connect_local("notify::percentage", false, clone!(@weak progressbar, @weak self.database as db => @default-return None::<glib::Value>, move |_|{
        //     progressbar.set_fraction(db.get_percentage());
        //     None
        // })).unwrap();

        // get_widget!(self.builder, gtk::Label, label);
        // self.database.connect_local("notify::remote", false, clone!(@weak label, @weak self.database as db => @default-return None::<glib::Value>, move |_|{
        //     if db.get_remote() != "" {
        //         label.set_text(&format!("Parsing metadata from remote \"{}\"", db.get_remote()));
        //     }else{
        //         label.set_text("");
        //     }
        //     None
        // })).unwrap();
    }
}
